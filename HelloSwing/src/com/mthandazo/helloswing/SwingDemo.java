package com.mthandazo.helloswing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SwingDemo implements ActionListener {
    public JLabel promptLbl;
    public JLabel contentLbl;
    public JButton reverseBtn;
    public JTextField userInput;
    public SwingDemo() {
        //Create a new Jframe container
        JFrame jFrame = new JFrame("String Reverse");

        // Define the FlowLayout
        FlowLayout flowLayout = new FlowLayout();

        //Specifiy the Flow Layout to the LayoutManager
        jFrame.setLayout(flowLayout);

        // Instantiate the label
        promptLbl = new JLabel("Enter a word");

        // Instantiate the contentLbl
        contentLbl = new JLabel("");

        // Instantiate the text field
        userInput = new JTextField(10);

        // set the actionCommand for the text field
        userInput.setActionCommand("textfield");

        // Instantiate the button
        reverseBtn = new JButton("Reverse");

        reverseBtn.addActionListener(this);
        userInput.addActionListener(this);


        // Add Event Listener to the First Button
//        first_button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.out.println("Hello from the first button");
//
//            }
//        });
//
//        // Add Event Listener to the Second Button
//        second_button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.out.println("Hello from the second button");
//            }
//        });

        // Define the size of the frame
        jFrame.setSize(250, 115);

        // Terminate the program when the user closes the application
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Add a components to the Jframe
        jFrame.add(promptLbl);
        jFrame.add(userInput);
        jFrame.add(reverseBtn);
        jFrame.add(contentLbl);

        // Spawn the Frame
        jFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand(); // get the action command

        if (actionCommand.equals("Transform")){
            // Perform an action when the reverse button is clicked
            String originalStr = userInput.getText();
            String holder = "";
            for (int i = originalStr.length() - 1; i >= 0; i--){
                holder += originalStr.charAt(i);
            }
            contentLbl.setText(holder);
        }
        if (actionCommand.equals("textfield")){
            // perform an action associated with the textfield
            contentLbl.setText("Original Text: " + userInput.getText());
        }

    }
}
