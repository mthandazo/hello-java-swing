package com.mthandazo.helloswing;
import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        //Create a thread to instantiate the Swing Demo class
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new SwingDemo();
            }
        });
    }
}
